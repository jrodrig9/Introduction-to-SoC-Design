# Introduction-to-SoC-Design-Education

More info about this ARM course [link](https://www.arm.com/resources/education/education-kits/introduction-to-soc)

## Requirements

Vivado 2022.1 and Keil uVision 6

## Learning Outcomes
```
    Knowledge and understanding of:
        The advantages and limitations of SoC.
        The key features of the Arm Cortex-M0 processor architecture.
        How to write simple and power efficient Arm assembly code using Arm instruction sets.
        The functions of the Address decoder and subordinate multiplexor in the AHB-Lite bus system
    Intellectual
        Outline the components of the AHB VGA peripheral and describe their functions.
        Outline the components of the AHB UART peripheral and describe their functions.
        Outline the components of the AHB Timer, GPIO and 7-segment display peripherals and describe their functions.
        Explain the concepts of interrupts and exceptions and the process by which the processor handles each.
        Describe the Cortex Microcontroller Software Interface Standard (CMSIS) and identify how to write device drivers to access AHB peripherals.
        Describe the advantages of the use of APIs over low -level programming.
    Practical
        Implement a simple SoC which consist of Cortex-M0 processor, AHB-Lite bus and AHB peripherals (Memory, LED, VGA) on an FPGA and write a simple program to display text on a connected VGA.
        Implement an SoC which contains a Cortex-M0 processor, AHB-Lite bus and AHB peripherals (Program memory and LED, VGA, UART, Timer, GPIO and 7-Segment) on an FPGA and write simple programs to control the peripherals.
        Implement the AHB timer and UART interrupt mechanism at both hardware and software domains by adding appropriate interrupt registers and writing suitable interrupt handler.
        Implement a timer interrupt handler and UART interrupt handler in a high-level language such as C. 
```

## Warning
This hardware proyect contains ARM intellectual property.

## Contributing
This is an public repository of Juan C. Rodríguez and is open for contributions.

