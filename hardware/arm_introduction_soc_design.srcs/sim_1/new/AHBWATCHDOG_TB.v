`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.08.2022 14:46:21
// Design Name: 
// Module Name: AHB2LED_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module AHBWATCHDOG_TB();

            reg HSEL;
		//Global Signal
			reg HCLK;
			reg HRESETn;
		//Address, Control & Write Data
			reg HREADY;
			reg [31:0] HADDR;
			reg [1:0] HTRANS;
			reg HWRITE;
			reg [2:0] HSIZE;
			
			reg [31:0] HWDATA;
		// Transfer Response & Read Data
			wire HREADYOUT;
			wire [31:0] HRDATA;
			
			wire RESET;


            AHBWATCHDOG TEST(
                //AHBLITE INTERFACE
                    //Slave Select Signals
                        .HSEL(HSEL),
                    //Global Signal
                        .HCLK(HCLK),
                        .HRESETn(HRESETn),
                    //Address, Control & Write Data
                        .HREADY(HREADY),
                        .HADDR(HADDR),
                        .HTRANS(HTRANS),
                        .HWRITE(HWRITE),
                        .HWDATA(HWDATA),
                    // Transfer Response & Read Data
                        .HREADYOUT(HREADYOUT),
                        .HRDATA(HRDATA),
                        .watchdog_reset(RESET)
            );
            
            always #5 HCLK = ~HCLK;
            
            initial 
            begin
		//Global Signal
			    HCLK = 0;
			    HRESETn  = 0;
			    HREADY = 1;
			    
		//Address, Control & Write Data
		        #45 HRESETn  = 1;
		        /*HSEL = 1;
                HADDR = 32'h54000000;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = 100;
                                
                #10 HADDR = 32'h54000008;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = 32'h00000001;
                
                /*#10 HADDR = 32'h52000010;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = 32'hfffffffa;
                
                #10 HADDR = 32'h52000010;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = 30;
                
                #10 HADDR = 32'h54000000;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 0;
                
                #10 HADDR = 32'h54000008;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 0;
                
                /*#10 HADDR = 32'h52000010;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 0;
                
                #10 HADDR = 32'h54000004;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 0;
                
                #300 HADDR = 32'h54000000;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = 100;
                
                #10 HADDR = 32'h54000004;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 0;
                
                #1000 HADDR = 32'h54000000;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = 100;
                
                #10 HADDR = 32'h54000004;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 0;*/
                
                #10000 $finish;
            
            end

endmodule