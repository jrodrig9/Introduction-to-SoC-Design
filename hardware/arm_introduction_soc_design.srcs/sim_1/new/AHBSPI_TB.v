`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 19.10.2022 14:13:15
// Design Name: 
// Module Name: AHBSPI_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module AHBSPI_TB();

   reg HSEL;
		//Global Signal
			reg HCLK;
			reg HRESETn;
		//Address, Control & Write Data
			reg HREADY;
			reg [31:0] HADDR;
			reg [1:0] HTRANS;
			reg HWRITE;
			reg [2:0] HSIZE;
			
			reg [31:0] HWDATA;
		// Transfer Response & Read Data
			wire HREADYOUT;
			wire [31:0] HRDATA;
			
			reg  miso;
			wire mosi;
			wire sclk;
			wire IRQ;
			reg [7:0] data;
			reg [2:0] counter;


            AHBSPI TEST(
                //AHBLITE INTERFACE
                    //Slave Select Signals
                        .HSEL(HSEL),
                    //Global Signal
                        .HCLK(HCLK),
                        .HRESETn(HRESETn),
                    //Address, Control & Write Data
                        .HREADY(HREADY),
                        .HADDR(HADDR),
                        .HTRANS(HTRANS),
                        .HWRITE(HWRITE),
                        .HWDATA(HWDATA),
                    // Transfer Response & Read Data
                        .miso(miso),
                        .mosi(mosi),
                        .sclk(sclk),
                        .HREADYOUT(HREADYOUT),
                        .HRDATA(HRDATA),
                        .spi_irq(IRQ)
            );
            
            always #5 HCLK = ~HCLK;
            
            always @( posedge sclk)
            begin
                miso = data[counter];
                counter = counter + 1'b1;
            end

            
            initial 
            begin
		//Global Signal
			    HCLK = 0;
			    HRESETn  = 0;
			    HREADY = 1;		
			    data = 8'b10101010;	   
			    counter = 3'b000; 
		//Address, Control & Write Data
		        #47 HRESETn  = 1;
		        HSEL = 1;
                HADDR = 32'h54000000;
                HTRANS = 2'b10;
                HWRITE = 1;
                #8 HWDATA = {24'h000000, 8'hAA};
                #1 HADDR = 32'h54000000;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = {24'h000000, 8'hFA};              
                #1 HADDR = 32'h54000004;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = {24'h000000, 8'h01};
                #1 HADDR = 32'h54000008;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = {24'h000000, 8'h02};
                #1 HADDR = 32'h5400000c;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 0;
               
                $finish;
            
            end
endmodule
