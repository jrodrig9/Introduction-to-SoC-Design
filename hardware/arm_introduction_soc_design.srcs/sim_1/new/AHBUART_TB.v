`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.08.2022 14:46:21
// Design Name: 
// Module Name: AHB2LED_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module AHBUART_TB();

            reg HSEL;
		//Global Signal
			reg HCLK;
			reg HRESETn;
		//Address, Control & Write Data
			reg HREADY;
			reg [31:0] HADDR;
			reg [1:0] HTRANS;
			reg HWRITE;
			reg [2:0] HSIZE;
			
			reg [31:0] HWDATA;
		// Transfer Response & Read Data
			wire HREADYOUT;
			wire [31:0] HRDATA;
		//LED Output
			wire RX;
			reg [9:0] data;
			wire TX;
			wire IRQ;

            AHBUART TEST(
                //AHBLITE INTERFACE
                    //Slave Select Signals
                        .HSEL(HSEL),
                    //Global Signal
                        .HCLK(HCLK),
                        .HRESETn(HRESETn),
                    //Address, Control & Write Data
                        .HREADY(HREADY),
                        .HADDR(HADDR),
                        .HTRANS(HTRANS),
                        .HWRITE(HWRITE),
                        
                        .HWDATA(HWDATA),
                    // Transfer Response & Read Data
                        .HREADYOUT(HREADYOUT),
                        .HRDATA(HRDATA),
                    //LED Output
                        .rx(RX),
                        .tx(TX),
                        .uart_irq(IRQ)
            );
            
            always #5 HCLK = ~HCLK;
            
            reg [21:0] count_reg;
            wire [21:0] count_next;
            reg [3:0]  position;
            reg [3:0]  position_next;
            reg [5:0]  b;
            reg [5:0]  b_next;
            
            //Counter
            always @ (posedge HCLK, negedge HRESETn)
              begin
                if(!HRESETn)
                begin
                  count_reg <= 0;
                  position <= 0;
                  b <= 0;
                end
                  
                else
                begin
                  count_reg <= count_next;
                  position <= position_next;
                  b <= b_next;
                end
            end
            
            //Baudrate  = 19200 = 50Mhz/(163*16)
            assign count_next = ((count_reg == 162) ? 0 : count_reg + 1'b1);
            assign baudtick = ((count_reg == 162) ? 1'b1 : 1'b0);
            assign RX = data[position];
            
            always @ (*)
            begin
                position_next = position;
                b_next = b;
                if(baudtick)
                begin
                 if(b_next == 15)
                 begin
                    position_next = position + 1;
                    b_next = 0;
                 end  
                 
                else
                begin
                    b_next = b + 1;
                end
               end
               
               if(position_next == 9)
                   position_next = 0;              
            end
            
            
            initial 
            begin
		//Global Signal
			    HCLK = 0;
			    HRESETn  = 0;
			    HREADY = 1;
			    data = 10'b0111111110;

		//Address, Control & Write Data
		        #45 HRESETn  = 1;
		        HSEL = 1;
                HADDR = 32'h51000008;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = 1;
                                
                /*#10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = 32'h01;
                HWRITE = 0;
                HTRANS = 0;
                
                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = 32'h02;
                HWRITE = 0;
                HTRANS = 0;
                
                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = 32'h03;
                HWRITE = 0;
                HTRANS = 0;

                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = 32'h04;
                HWRITE = 0;
                HTRANS = 0;
                
                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = 32'h05;
                HWRITE = 0;
                HTRANS = 0;
                
                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = 32'h06;
                HWRITE = 0;
                HTRANS = 0;
                
                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = 32'h07;
                HWRITE = 0;
                HTRANS = 0;
                
                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = 32'h08;
                HWRITE = 0;
                HTRANS = 0;

                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = 32'h09;
                HWRITE = 0;
                HTRANS = 0;
                
                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = 32'h0A;
                HWRITE = 0;
                HTRANS = 0;
                
                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = 32'h0B;
                HWRITE = 0;
                HTRANS = 0;
                
                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = 32'h0C;
                HWRITE = 0;
                HTRANS = 0;
                
                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = 32'h0D;
                HWRITE = 0;
                HTRANS = 0;

                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = 32'h0E;
                HWRITE = 0;
                HTRANS = 0;
                
                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = 32'h0F;
                HWRITE = 0;
                HTRANS = 0;
                                
                 #10 HADDR = 32'h51000004;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 32'h00;
                
                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 32'h00;
                HWRITE = 0;
                HTRANS = 0;
                
                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 32'h00;
                HWRITE = 0;
                HTRANS = 0;
                
                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 32'h00;
                HWRITE = 0;
                HTRANS = 0;

                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 32'h00;
                HWRITE = 0;
                HTRANS = 0;
                
                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 32'h00;
                HWRITE = 0;
                HTRANS = 0;
                
                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 32'h00;
                HWRITE = 0;
                HTRANS = 0;
                
                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 32'h00;
                HWRITE = 0;
                HTRANS = 0;
                
                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 32'h00;
                HWRITE = 0;
                HTRANS = 0;
                
                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 32'h00;
                HWRITE = 0;
                HTRANS = 0;
                         
                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 32'h00;
                HWRITE = 0;
                HTRANS = 0;
                
                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 32'h00;
                HWRITE = 0;
                HTRANS = 0;
                
                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 32'h00;
                HWRITE = 0;
                HTRANS = 0;
                
                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 32'h00;
                HWRITE = 0;
                HTRANS = 0;
                
                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 32'h00;
                HWRITE = 0;
                HTRANS = 0;
                
                #10 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 32'h00;
                HWRITE = 0;
                HTRANS = 0;
                
                #10 HADDR = 32'h51000004;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 32'h00;
                
                #200 HADDR = 32'h51000004;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 32'h00;
                
                #300000 HADDR = 32'h51000000;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 32'h00;
                HWRITE = 0;
                HTRANS = 0;*/
                
                #200 HADDR = 32'h51000004;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 32'h00;
                
                
               //#100000000 $finish;
            
            end

endmodule