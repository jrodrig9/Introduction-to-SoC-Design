`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.08.2022 14:46:21
// Design Name: 
// Module Name: AHB2LED_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module AHBGPIO_TB();

            reg HSEL;
		//Global Signal
			reg HCLK;
			reg HRESETn;
		//Address, Control & Write Data
			reg HREADY;
			reg [31:0] HADDR;
			reg [1:0] HTRANS;
			reg HWRITE;
			reg [2:0] HSIZE;
			
			reg [31:0] HWDATA;
		// Transfer Response & Read Data
			wire HREADYOUT;
			wire [31:0] HRDATA;
			
		    reg [3:0] GPIO_IN;
			wire  [3:0] GPIO_OUT;
			wire IRQ;


            AHBGPIO TEST(
                //AHBLITE INTERFACE
                    //Slave Select Signals
                        .HSEL(HSEL),
                    //Global Signal
                        .HCLK(HCLK),
                        .HRESETn(HRESETn),
                    //Address, Control & Write Data
                        .HREADY(HREADY),
                        .HADDR(HADDR),
                        .HTRANS(HTRANS),
                        .HWRITE(HWRITE),
                        .HWDATA(HWDATA),
                        .gpio_in(GPIO_IN),
                    // Transfer Response & Read Data
                        .gpio_out(GPIO_OUT),
                        .HREADYOUT(HREADYOUT),
                        .HRDATA(HRDATA),
                        .gpio_irq(IRQ)
            );
            
            always #5 HCLK = ~HCLK;
            
            initial 
            begin
		//Global Signal
			    HCLK = 0;
			    HRESETn  = 0;
			    HREADY = 1;
			    GPIO_IN = 1;
			    
		//Address, Control & Write Data
		        #45 HRESETn  = 1;
		        HSEL = 1;
                GPIO_IN = 3;
                 
                #45 GPIO_IN = 4;
                
                #1000 HADDR = 32'h5300000c;
                HTRANS = 2'b10;
                HWRITE = 1;
                #10 HWDATA = 1;
                
                #45 HADDR = 32'h53000000;
                HTRANS = 2'b10;
                HWRITE = 0;
                #10 HWDATA = 0;
                
                #45 GPIO_IN = 5;
               
                $finish;
            
            end

endmodule