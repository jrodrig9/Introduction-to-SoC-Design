//////////////////////////////////////////////////////////////////////////////////
//END USER LICENCE AGREEMENT                                                    //
//                                                                              //
//Copyright (c) 2012, ARM All rights reserved.                                  //
//                                                                              //
//THIS END USER LICENCE AGREEMENT ("LICENCE") IS A LEGAL AGREEMENT BETWEEN      //
//YOU AND ARM LIMITED ("ARM") FOR THE USE OF THE SOFTWARE EXAMPLE ACCOMPANYING  //
//THIS LICENCE. ARM IS ONLY WILLING TO LICENSE THE SOFTWARE EXAMPLE TO YOU ON   //
//CONDITION THAT YOU ACCEPT ALL OF THE TERMS IN THIS LICENCE. BY INSTALLING OR  //
//OTHERWISE USING OR COPYING THE SOFTWARE EXAMPLE YOU INDICATE THAT YOU AGREE   //
//TO BE BOUND BY ALL OF THE TERMS OF THIS LICENCE. IF YOU DO NOT AGREE TO THE   //
//TERMS OF THIS LICENCE, ARM IS UNWILLING TO LICENSE THE SOFTWARE EXAMPLE TO    //
//YOU AND YOU MAY NOT INSTALL, USE OR COPY THE SOFTWARE EXAMPLE.                //
//                                                                              //
//ARM hereby grants to you, subject to the terms and conditions of this Licence,//
//a non-exclusive, worldwide, non-transferable, copyright licence only to       //
//redistribute and use in source and binary forms, with or without modification,//
//for academic purposes provided the following conditions are met:              //
//a) Redistributions of source code must retain the above copyright notice, this//
//list of conditions and the following disclaimer.                              //
//b) Redistributions in binary form must reproduce the above copyright notice,  //
//this list of conditions and the following disclaimer in the documentation     //
//and/or other materials provided with the distribution.                        //
//                                                                              //
//THIS SOFTWARE EXAMPLE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ARM     //
//EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING     //
//WITHOUT LIMITATION WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR //
//PURPOSE, WITH RESPECT TO THIS SOFTWARE EXAMPLE. IN NO EVENT SHALL ARM BE LIABLE/
//FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE, OR CONSEQUENTIAL DAMAGES OF ANY/
//KIND WHATSOEVER WITH RESPECT TO THE SOFTWARE EXAMPLE. ARM SHALL NOT BE LIABLE //
//FOR ANY CLAIMS, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, //
//TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE    //
//EXAMPLE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE EXAMPLE. FOR THE AVOIDANCE/
// OF DOUBT, NO PATENT LICENSES ARE BEING LICENSED UNDER THIS LICENSE AGREEMENT.//
//////////////////////////////////////////////////////////////////////////////////
module VGAInterface(
    input CLK,
    input RESET,
    input [7:0] COLOUR_IN,
    input [7:0] resolution,
    output wire [7:0] cout,
    output wire hs,
    output wire vs,
    output wire [9:0] addrh,
    output wire [9:0] addrv,
    output wire [9:0] res_x,
    output wire [9:0] res_y
    );
    
    wire [7:0] cout_r0;
    wire hs_r0;
    wire vs_r0;
    wire [9:0] addrh_r0;
    wire [9:0] addrv_r0;
    
    wire [7:0] cout_r1;
    wire hs_r1;
    wire vs_r1;
    wire [9:0] addrh_r1;
    wire [9:0] addrv_r1;
    
    wire [7:0] cout_r2;
    wire hs_r2;
    wire vs_r2;
    wire [9:0] addrh_r2;
    wire [9:0] addrv_r2;
    
   vga_resolution_640_480 resolution_0(
      .CLK(CLK),
      .RESET(RESET),
      .COLOUR_IN(COLOUR_IN),
      .cout(cout_r0),
      .hs(hs_r0),
      .vs(vs_r0),
      .addrh(addrh_r0),
      .addrv(addrv_r0)
    );
    
 vga_resolution_640_400 resolution_1(
      .CLK(CLK),
      .RESET(RESET),
      .COLOUR_IN(COLOUR_IN),
      .cout(cout_r1),
      .hs(hs_r1),
      .vs(vs_r1),
      .addrh(addrh_r1),
      .addrv(addrv_r1)
    );   
    
 vga_resolution_640_350 resolution_2(
      .CLK(CLK),
      .RESET(RESET),
      .COLOUR_IN(COLOUR_IN),
      .cout(cout_r2),
      .hs(hs_r2),
      .vs(vs_r2),
      .addrh(addrh_r2),
      .addrv(addrv_r2)
    );      
    
 RESOLUTIONMUX uRESOLUTIONMUX(
  .RESOLUTION(resolution),
    
  .cout_r0(cout_r0),
  .hs_r0(hs_r0),
  .vs_r0(vs_r0),
  .addrh_r0(addrh_r0),
  .addrv_r0(addrv_r0),
  
  .cout_r1(cout_r1),
  .hs_r1(hs_r1),
  .vs_r1(vs_r1),
  .addrh_r1(addrh_r1),
  .addrv_r1(addrv_r1),
  
  .cout_r2(cout_r2),
  .hs_r2(hs_r2),
  .vs_r2(vs_r2),
  .addrh_r2(addrh_r2),
  .addrv_r2(addrv_r2),
  
  .cout(cout),
  .hs(hs),
  .vs(vs),
  .addrh(addrh),
  .addrv(addrv),
  .res_x(res_x),
  .res_y(res_y)
); 

endmodule
