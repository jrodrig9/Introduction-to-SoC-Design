//////////////////////////////////////////////////////////////////////////////////
//END USER LICENCE AGREEMENT                                                    //
//                                                                              //
//Copyright (c) 2012, ARM All rights reserved.                                  //
//                                                                              //
//THIS END USER LICENCE AGREEMENT (“LICENCE”) IS A LEGAL AGREEMENT BETWEEN      //
//YOU AND ARM LIMITED ("ARM") FOR THE USE OF THE SOFTWARE EXAMPLE ACCOMPANYING  //
//THIS LICENCE. ARM IS ONLY WILLING TO LICENSE THE SOFTWARE EXAMPLE TO YOU ON   //
//CONDITION THAT YOU ACCEPT ALL OF THE TERMS IN THIS LICENCE. BY INSTALLING OR  //
//OTHERWISE USING OR COPYING THE SOFTWARE EXAMPLE YOU INDICATE THAT YOU AGREE   //
//TO BE BOUND BY ALL OF THE TERMS OF THIS LICENCE. IF YOU DO NOT AGREE TO THE   //
//TERMS OF THIS LICENCE, ARM IS UNWILLING TO LICENSE THE SOFTWARE EXAMPLE TO    //
//YOU AND YOU MAY NOT INSTALL, USE OR COPY THE SOFTWARE EXAMPLE.                //
//                                                                              //
//ARM hereby grants to you, subject to the terms and conditions of this Licence,//
//a non-exclusive, worldwide, non-transferable, copyright licence only to       //
//redistribute and use in source and binary forms, with or without modification,//
//for academic purposes provided the following conditions are met:              //
//a) Redistributions of source code must retain the above copyright notice, this//
//list of conditions and the following disclaimer.                              //
//b) Redistributions in binary form must reproduce the above copyright notice,  //
//this list of conditions and the following disclaimer in the documentation     //
//and/or other materials provided with the distribution.                        //
//                                                                              //
//THIS SOFTWARE EXAMPLE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ARM     //
//EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING     //
//WITHOUT LIMITATION WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR //
//PURPOSE, WITH RESPECT TO THIS SOFTWARE EXAMPLE. IN NO EVENT SHALL ARM BE LIABLE/
//FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE, OR CONSEQUENTIAL DAMAGES OF ANY/
//KIND WHATSOEVER WITH RESPECT TO THE SOFTWARE EXAMPLE. ARM SHALL NOT BE LIABLE //
//FOR ANY CLAIMS, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, //
//TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE    //
//EXAMPLE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE EXAMPLE. FOR THE AVOIDANCE/
// OF DOUBT, NO PATENT LICENSES ARE BEING LICENSED UNDER THIS LICENSE AGREEMENT.//
//////////////////////////////////////////////////////////////////////////////////


module AHBVGA(
  input wire HCLK,
  input wire HRESETn,
  input wire [31:0] HADDR,
  input wire [31:0] HWDATA,
  input wire HREADY,
  input wire HWRITE,
  input wire [1:0] HTRANS,
  input wire HSEL,
  
  output reg [31:0] HRDATA,
  output wire HREADYOUT,
  
  output wire hsync,
  output wire vsync,
  output wire [7:0] rgb
);
  //Register locations
  localparam IMAGEADDR = 4'hA;
  localparam CONSOLEADDR = 4'h0;
  
  //Internal AHB signals
  reg last_HWRITE;
  reg last_HSEL;
  reg [1:0] last_HTRANS;
  reg [31:0] last_HADDR;
  
  wire [7:0] console_rgb; //console rgb signal              
  wire [9:0] pixel_x;     //current x pixel
  wire [9:0] pixel_y;     //current y pixel
  
  reg console_write;      //write to console
  reg [7:0] console_wdata;//data to write to console
  reg image_write;        //write to image
  reg [7:0] image_wdata;  //data to write to image
  
  wire [7:0] image_rgb;   //image color
  
  wire scroll;            //scrolling signal
  
  reg [7:0] resolution;
  wire sel_resolution;
  wire [9:0] res_x;    
  wire [9:0] res_y;     
  
  wire sel_split;       
  wire sel_console;       
  wire sel_image;
  
  reg [7:0] cin;
  
  reg [7:0] split;
  wire [7:0] MAX_X;
  wire [7:0] MAX_Y;
  wire [15:0] split_size;
  
  wire RESET; 
  
  initial
  begin
    resolution = 0;
    split = 0;
  end
  
  always @(posedge HCLK)
  if(HREADY)
    begin
      last_HADDR <= HADDR;
      last_HWRITE <= HWRITE;
      last_HSEL <= HSEL;
      last_HTRANS <= HTRANS;
    end
    
  //Give time for the screen to refresh before writing
  assign HREADYOUT = ~scroll;   
  
  SPLITDEC uSPLITDEC
  (
    .split(split),
    .res_y(res_y),
    .split_size(split_size),
    .MAX_X(MAX_X),
    .MAX_Y(MAX_Y)
  );
 
  //VGA interface: control the synchronization and color signals for a particular resolution
  VGAInterface uVGAInterface (
    .CLK(HCLK),
    .RESET(RESET), 
    .COLOUR_IN(cin),
    .resolution(resolution), 
    .cout(rgb), 
    .hs(hsync), 
    .vs(vsync), 
    .addrh(pixel_x), 
    .addrv(pixel_y),
    .res_x(res_x),
    .res_y(res_y)
    );

  //VGA console module: output the pixels in the text region
  vga_console uvga_console(
    .clk(HCLK),
    .resetn(HRESETn),
    .MAX_X(MAX_X),
    .MAX_Y(MAX_Y),
    .pixel_x(pixel_x),
    .pixel_y(pixel_y),
    .text_rgb(console_rgb),
    .font_we(console_write),
    .font_data(console_wdata),
    .scroll(scroll)
    );
  
  //VGA image buffer: output the pixels in the image region
  vga_image uvga_image(
    .clk(HCLK),
    .resetn(HRESETn),
    .split_size(split_size),
    .address(last_HADDR[15:2]),
    .pixel_x(pixel_x),
    .pixel_y(pixel_y),
    .image_we(image_write),
    .image_data(image_wdata),
    .image_rgb(image_rgb)
    );

  assign sel_resolution = (last_HADDR[23:0] == 12'h000000000000);
  assign sel_split = (last_HADDR[23:0] == 12'h000000000004);
  assign sel_console = (last_HADDR[23:0] == 12'h000000000008);
  assign sel_image = (last_HADDR[23:0] > 12'h000000000008);
  assign RESET = 0;
  
  always @(*)
  begin
    if(sel_resolution)
        HRDATA[7:0] <= resolution;
    else if(sel_split)
        HRDATA[15:0] <= split;
    else
        HRDATA <= 0;    
  end
  
  //Set console write and write data
  always @(posedge HCLK, negedge HRESETn)
  begin
    if(!HRESETn)
      begin
        console_write <= 0;
        console_wdata <= 0;
      end
    else if(last_HWRITE & last_HSEL & last_HTRANS[1] & HREADYOUT & sel_console)
      begin
        console_write <= 1'b1;
        console_wdata <= HWDATA[7:0];
      end
    else
      begin
        console_write <= 1'b0;
        console_wdata <= 0;                 
      end
  end
  
  //Set image write and image write data
  always @(posedge HCLK, negedge HRESETn)
  begin
    if(!HRESETn)
      begin
        image_write <= 0;
        image_wdata <= 0;
      end
    else if(last_HWRITE & last_HSEL & last_HTRANS[1] & HREADYOUT & sel_image)
      begin
        image_write <= 1'b1;
        image_wdata <= HWDATA[7:0];
      end
    else
      begin
        image_write <= 1'b0;
        image_wdata <= 0;
      end
  end
  
  always @(posedge HCLK, negedge HRESETn)
  begin
    if(!HRESETn)
      begin
        resolution <= 0;
      end
      
    else if(last_HWRITE & last_HSEL & last_HTRANS[1] & HREADYOUT & sel_resolution)
      begin
        resolution <= HWDATA[7:0];
      end
  end
  
  always @(posedge HCLK, negedge HRESETn)
  begin
    if(!HRESETn)
      begin
        split <= 0;
      end
      
    else if(last_HWRITE & last_HSEL & last_HTRANS[1] & HREADYOUT & sel_split)
      begin
        split <= HWDATA[7:0];
      end
  end
  
  //Select the rgb color for a particular region
  always @*
  begin
    if(!HRESETn)
      cin <= 8'h00;
    else 
      if(pixel_x[9:0]< split_size )
        cin <= console_rgb ;
      else
        cin <= image_rgb;
  end

endmodule
  
  
