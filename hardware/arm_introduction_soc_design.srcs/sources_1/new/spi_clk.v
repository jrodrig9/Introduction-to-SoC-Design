//////////////////////////////////////////////////////////////////////////////////
//END USER LICENCE AGREEMENT                                                    //
//                                                                              //
//Copyright (c) 2012, ARM All rights reserved.                                  //
//                                                                              //
//THIS END USER LICENCE AGREEMENT ("LICENCE") IS A LEGAL AGREEMENT BETWEEN      //
//YOU AND ARM LIMITED ("ARM") FOR THE USE OF THE SOFTWARE EXAMPLE ACCOMPANYING  //
//THIS LICENCE. ARM IS ONLY WILLING TO LICENSE THE SOFTWARE EXAMPLE TO YOU ON   //
//CONDITION THAT YOU ACCEPT ALL OF THE TERMS IN THIS LICENCE. BY INSTALLING OR  //
//OTHERWISE USING OR COPYING THE SOFTWARE EXAMPLE YOU INDICATE THAT YOU AGREE   //
//TO BE BOUND BY ALL OF THE TERMS OF THIS LICENCE. IF YOU DO NOT AGREE TO THE   //
//TERMS OF THIS LICENCE, ARM IS UNWILLING TO LICENSE THE SOFTWARE EXAMPLE TO    //
//YOU AND YOU MAY NOT INSTALL, USE OR COPY THE SOFTWARE EXAMPLE.                //
//                                                                              //
//ARM hereby grants to you, subject to the terms and conditions of this Licence,//
//a non-exclusive, worldwide, non-transferable, copyright licence only to       //
//redistribute and use in source and binary forms, with or without modification,//
//for academic purposes provided the following conditions are met:              //
//a) Redistributions of source code must retain the above copyright notice, this//
//list of conditions and the following disclaimer.                              //
//b) Redistributions in binary form must reproduce the above copyright notice,  //
//this list of conditions and the following disclaimer in the documentation     //
//and/or other materials provided with the distribution.                        //
//                                                                              //
//THIS SOFTWARE EXAMPLE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ARM     //
//EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING     //
//WITHOUT LIMITATION WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR //
//PURPOSE, WITH RESPECT TO THIS SOFTWARE EXAMPLE. IN NO EVENT SHALL ARM BE LIABLE/
//FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE, OR CONSEQUENTIAL DAMAGES OF ANY/
//KIND WHATSOEVER WITH RESPECT TO THE SOFTWARE EXAMPLE. ARM SHALL NOT BE LIABLE //
//FOR ANY CLAIMS, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, //
//TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE    //
//EXAMPLE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE EXAMPLE. FOR THE AVOIDANCE/
// OF DOUBT, NO PATENT LICENSES ARE BEING LICENSED UNDER THIS LICENSE AGREEMENT.//
//////////////////////////////////////////////////////////////////////////////////

module SPI_CLK
(
  input wire clk,
  input wire resetn,
  output wire  s_tick_miso,
  output wire  s_tick_mosi,
  output reg  sclk
);

reg sclk_next;
reg [23:0] count_sclk_reg;
reg [23:0] count_sclk_next;
reg [23:0] count_s_tick_reg;
reg [23:0] count_s_tick_next;
reg [23:0] count_s_tick_mosi_reg;
reg [23:0] count_s_tick_mosi_next;

assign s_tick_miso = (count_s_tick_reg == 24'h00002) ? 1'b1 : 1'b0;
assign s_tick_mosi = (count_s_tick_mosi_reg == 24'h00002) ? 1'b1 : 1'b0;

//Counter
always @ (posedge clk, negedge resetn)
  begin
    if(!resetn)
    begin
        count_sclk_reg <= 1'b1;
        count_s_tick_reg <= 0;
        count_s_tick_mosi_reg <= 1;
        sclk <= 1;
    end
      
    else
    begin
        count_sclk_reg <= count_sclk_next;
        count_s_tick_reg <= count_s_tick_next;
        count_s_tick_mosi_reg <= count_s_tick_mosi_next;
        sclk <= sclk_next;
    end
end

always @ (*)
    begin
      count_sclk_next = count_sclk_reg;
      count_s_tick_next = count_s_tick_reg;
      count_s_tick_mosi_next = count_s_tick_mosi_reg;
      sclk_next = sclk;
      if(count_s_tick_reg == 24'h00002)
        count_s_tick_next = 0;
       if(count_s_tick_mosi_reg == 24'h00002)
        count_s_tick_mosi_next = 0;
      if(count_sclk_reg == 24'h7A120)
      begin
        count_sclk_next = 0;
        sclk_next = sclk + 1'b1;
        count_s_tick_next = count_s_tick_reg + 1'b1;
        count_s_tick_mosi_next = count_s_tick_mosi_reg + 1'b1;
      end
        
      else
        count_sclk_next = count_sclk_reg + 1'b1;
    end

endmodule
