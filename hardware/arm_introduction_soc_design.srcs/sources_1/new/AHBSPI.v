module AHBSPI(
  input wire HCLK,
  input wire HRESETn,
  input wire [31:0] HADDR,
  input wire [31:0] HWDATA,
  input wire HREADY,
  input wire HWRITE,
  input wire [1:0] HTRANS,
  input wire HSEL,
  
  input  wire miso,
  output wire sclk,
  output wire mosi,
  output reg [31:0] HRDATA,
  output wire HREADYOUT,
  //UART Interrupt
  output wire spi_irq  //Interrupt
);

 //Internal AHB signals
  reg last_HWRITE;
  reg last_HSEL;
  reg [1:0] last_HTRANS;
  reg [31:0] last_HADDR;
 
  wire s_tick;
  wire s_tick_mosi;
  wire sclk_base;
  wire add_mosi_data_fifo;
  wire get_miso_data_fifo;
  wire add_mosi_size_fifo;
  wire add_miso_size_fifo;
 
  wire mosi_data_done;
  wire miso_data_done;
  wire spi_done;
 
  wire [7:0] status;
  wire [7:0] rd_data;
  wire [7:0] wr_data;
  wire [7:0] mosi_data;
  wire [7:0] miso_data;
  
  wire [7:0] mosi_size;
  wire [7:0] miso_size;
  
  wire mosi_data_full;
  wire mosi_data_empty;
  
  wire mosi_size_full;
  wire mosi_size_empty;
  
  wire miso_data_full;
  wire miso_data_empty;
  
  wire miso_size_full;
  wire miso_size_empty;
  
  assign status = {4'b0000, mosi_size_full, miso_size_full, mosi_data_full,miso_data_empty};
  assign add_mosi_data_fifo = (last_HADDR[7:0] == 8'h00) & last_HWRITE & last_HSEL & last_HTRANS[1];
  assign get_miso_data_fifo = (last_HADDR[7:0] == 8'h00) & ~last_HWRITE & last_HSEL & last_HTRANS[1];
  assign add_mosi_size_fifo = (last_HADDR[7:0] == 8'h04) & last_HWRITE & last_HSEL & last_HTRANS[1];
  assign add_miso_size_fifo = (last_HADDR[7:0] == 8'h08) & last_HWRITE & last_HSEL & last_HTRANS[1];
  assign sel_status = (last_HADDR[7:0] == 8'h0c) & ~last_HWRITE & last_HSEL & last_HTRANS[1];
  assign wr_data = HWDATA[7:0];
   //assign spi_irq = ; controller
  assign HREADYOUT = ~mosi_data_full;

  always @(*)
  begin
    if(get_miso_data_fifo)
        HRDATA = {24'h000000, rd_data};
    else if(sel_status)
        HRDATA = {24'h000000, status};
    else
        HRDATA <= 0;    
  end  
  
  always @(posedge HCLK)
  if(HREADY)
    begin
      last_HADDR <= HADDR;
      last_HWRITE <= HWRITE;
      last_HSEL <= HSEL;
      last_HTRANS <= HTRANS;
    end
    
  SPI_CLK uSPI_CLK(
    .clk(HCLK),
    .resetn(HRESETn),
    .s_tick_miso(s_tick),
    .s_tick_mosi(s_tick_mosi),
    .sclk(sclk_base)
  );
   
   //Transmitter FIFO
  FIFO  
   #(.DWIDTH(8), .AWIDTH(2))
	uFIFO_MOSI 
  (
    .clk(HCLK),
    .resetn(HRESETn),
    .rd(mosi_data_done),//controller
    .wr(add_mosi_data_fifo),
    .w_data(wr_data[7:0]),
    .empty(mosi_data_empty),
    .full(mosi_data_full),
    .r_data(mosi_data[7:0])
  );
  
   //RECEIVER FIFO
  FIFO  
   #(.DWIDTH(8), .AWIDTH(2))
	uFIFO_MISO 
  (
    .clk(HCLK),
    .resetn(HRESETn),
    .rd(get_miso_data_fifo),
    .wr(miso_data_done),//controller
    .w_data(miso_data[7:0]),
    .empty(miso_data_empty),
    .full(miso_data_full),
    .r_data(rd_data[7:0])
  );
  
   //TRANSMITTER SIZE FIFO
  FIFO  
   #(.DWIDTH(8), .AWIDTH(2))
	uFIFO_MOSI_SIZE 
  (
    .clk(HCLK),
    .resetn(HRESETn),
    .rd(spi_irq),//controller
    .wr(add_mosi_size_fifo),//AHB signal
    .w_data(wr_data[7:0]),
    .empty(mosi_size_empty),
    .full(mosi_size_full),
    .r_data(mosi_size[7:0])
  );
  
   //RECEIVER SIZE FIFO
  FIFO  
   #(.DWIDTH(8), .AWIDTH(2))
	uFIFO_MISO_SIZE 
  (
    .clk(HCLK),
    .resetn(HRESETn),
    .rd(spi_irq),//controller
    .wr(add_miso_size_fifo),//AHB signal
    .w_data(wr_data[7:0]),
    .empty(miso_size_empty),
    .full(miso_size_full),
    .r_data(miso_size[7:0])
  );
  
  SPI_CTRL uSPI_CTRL(
  .clk(HCLK),
  .resetn(HRESETn),
  .s_tick(s_tick),
  .s_tick_mosi(s_tick_mosi), 
  .sclk_base(sclk_base),       //SPI generator tick
  .spi_start(!mosi_data_empty),
  .mosi_data(mosi_data), 
  .mosi_size(mosi_size), 
  .miso_size(miso_size), 
  .miso(miso),            
  
  .sclk(sclk),
  .mosi(mosi),
  .mosi_data_done(mosi_data_done),
  .miso_data_done(miso_data_done),       //transfer completed
  .spi_done(spi_irq),
  .miso_data(miso_data)    //output data
  );
  
  
endmodule
