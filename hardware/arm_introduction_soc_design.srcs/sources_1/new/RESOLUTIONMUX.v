module RESOLUTIONMUX(
  input wire [7:0] RESOLUTION,
    
  input wire [7:0] cout_r0,
  input wire hs_r0,
  input wire vs_r0,
  input wire [9:0] addrh_r0,
  input wire [9:0] addrv_r0,
  
  input wire [7:0] cout_r1,
  input wire hs_r1,
  input wire vs_r1,
  input wire [9:0] addrh_r1,
  input wire [9:0] addrv_r1,
  
  input wire [7:0] cout_r2,
  input wire hs_r2,
  input wire vs_r2,
  input wire [9:0] addrh_r2,
  input wire [9:0] addrv_r2,
  
  output reg RESET,
  output reg [7:0] cout,
  output reg hs,
  output reg vs,
  output reg [9:0] addrh,
  output reg [9:0] addrv,
  output reg [9:0] res_x,
  output reg [9:0] res_y
);


  always@*
  begin
    case(RESOLUTION)
      4'b0000: begin						// SELECT SLAVE0 RESPONSE & DATA IF PREVIOUS APHASE WAS FOR S0
        cout = cout_r0;
        hs = hs_r0;
        vs = vs_r0;
        addrh = addrh_r0;
        addrv = addrv_r0;
        res_x = 640;
        res_y = 480;
      end
      4'b0001: begin
        cout = cout_r1;
        hs = hs_r1;
        vs = vs_r1;
        addrh = addrh_r1;
        addrv = addrv_r1;
        res_x = 640;
        res_y = 400;
      end
      4'b0010: begin
        cout = cout_r2;
        hs = hs_r2;
        vs = vs_r2;
        addrh = addrh_r2;
        addrv = addrv_r2;
        res_x = 640;
        res_y = 350;
      end
    endcase
    
  end

endmodule
