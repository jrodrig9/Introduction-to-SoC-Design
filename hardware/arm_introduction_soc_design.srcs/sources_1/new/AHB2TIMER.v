module AHBTIMER(
	//Inputs
    input wire HCLK,
	input wire HRESETn,
    input wire [31:0] HADDR,
    input wire [31:0] HWDATA,
    input wire [1:0] HTRANS,
    input wire HWRITE,
    input wire HSEL,
    input wire HREADY,
    input wire capture_pin,
  
	//Output
    output reg [31:0] HRDATA,
    output wire HREADYOUT,
    output wire pwm,
    //UART Interrupt
    output reg timer_irq  //Interrupt
);

    localparam  idle_st =  1'b0;
    localparam  counter_st = 1'b1;
    
    localparam [3:0] load_m    = 4'b0000;
    localparam [3:0] free_m    = 4'b0001;
    localparam [3:0] capture_m = 4'b0011;
    localparam [3:0] compare_m = 4'b0010;
    localparam [3:0] pwm_m     = 4'b0110;

    //Internal AHB signals
    reg last_HWRITE;
    reg last_HSEL;
    reg [1:0] last_HTRANS;
    reg [31:0] last_HADDR;
    
    reg state;
    reg state_next;
    reg pwm_;
    reg pwm_next;
    reg timer_irq_next;

    reg  [31:0] load;
    reg  [31:0] timer;
    reg  [31:0] timer_next;
    reg  [31:0] control;
    reg  [31:0] capture;
    reg  [31:0] capture_next;
    reg  [31:0] compare;
    reg  clear;
    
    wire enable;
    wire  [3:0] mode;
    wire  [3:0] prescale;
    
    wire sel_load;
    wire sel_current;
    wire sel_control;
    wire sel_capture;
    wire sel_compare;
    wire sel_clear;
    wire capture_event;
    
    wire outclk;
        
    assign enable   = control[0];
    assign mode     = control[4:1];
    assign prescale = control [8:5];
    assign sel_load    = (last_HADDR[7:0] == 8'h00) & last_HSEL & last_HTRANS[1];
    assign sel_current = (last_HADDR[7:0] == 8'h04) & last_HSEL & last_HTRANS[1];
    assign sel_control = (last_HADDR[7:0] == 8'h08) & last_HSEL & last_HTRANS[1];
    assign sel_capture = (last_HADDR[7:0] == 8'h0c) & last_HSEL & last_HTRANS[1];
    assign sel_compare = (last_HADDR[7:0] == 8'h10) & last_HSEL & last_HTRANS[1]; 
    assign sel_clear   = (last_HADDR[7:0] == 8'h14) & last_HSEL & last_HTRANS[1]; 
    assign capture_event = (sel_capture & last_HWRITE) | capture_pin;
    assign pwm = pwm_;
    assign HREADYOUT = 1'b1;
   
  always @(*)
  begin
    if(sel_current & ~last_HWRITE)
        HRDATA = timer;
    else if(sel_control  & ~last_HWRITE)
        HRDATA = control;
    else if(sel_load  & ~last_HWRITE)
        HRDATA = load;
    else if(sel_capture  & ~last_HWRITE)
        HRDATA = capture;
    else if(sel_compare  & ~last_HWRITE)
        HRDATA = compare;
    else
        HRDATA = 0;    
  end 
  
  prescaler uprescaler
  (
    .inclk(HCLK),
    .resetn(HRESETn),
    .prescale(prescale),
    .outclk(outclk)
  );
     
  always @(posedge HCLK)
  if(HREADY)
    begin
      last_HADDR <= HADDR;
      last_HWRITE <= HWRITE;
      last_HSEL <= HSEL;
      last_HTRANS <= HTRANS;
    end
    
  always @(posedge HCLK, negedge HRESETn)
  begin
    if(!HRESETn)
      begin
        load <= 0;
      end
      
    else if(HREADYOUT & last_HWRITE & sel_load)
      begin
        load <= HWDATA;
      end
  end
   
  always @(posedge HCLK, negedge HRESETn)
  begin
    if(!HRESETn)
      begin
        control <= 0;
      end
      
    else if(HREADYOUT & last_HWRITE & sel_control)
      begin
        control <= HWDATA;
      end
  end
  
  always @(posedge HCLK, negedge HRESETn)
  begin
    if(!HRESETn)
      begin
        compare <= 0;
      end
      
    else if(HREADYOUT & last_HWRITE & sel_compare)
      begin
        compare <= HWDATA;
      end
  end
  
  always @(posedge HCLK, negedge HRESETn)
  begin
    if(!HRESETn)
      begin
        clear <= 0;
      end
      
    else if(HREADYOUT & last_HWRITE & sel_clear)
      begin
        clear <= HWDATA[0];
      end
  end
  
  //Machine Status for timer modes
  always @(posedge HCLK, negedge HRESETn)
  begin
    if(!HRESETn)
      begin
        state <= 0;
        timer <= 0;
        capture <= 0;
        pwm_ <= 0;
        timer_irq <= 0;
      end
      
    else
      begin
        state <= state_next;
        timer <= timer_next;
        capture <= capture_next;
        pwm_ <= pwm_next;
        timer_irq <= timer_irq_next;
      end
  end
  
  //State logic
  always@(*)
  begin
    state_next = state;
    timer_next = timer;
    pwm_next = pwm_;
    timer_irq_next = (clear) ? 0 : timer_irq;
    
    if(capture_event & (mode == capture_m))
    begin
        capture_next = timer;
        timer_irq_next = 1'b1;
    end 
    else
        capture_next = capture; 
          
    case(state)
        idle_st:
        begin
            if(enable & outclk)
            begin
                state_next = counter_st;
                timer_next = 0;
            end
        end
    
        counter_st:
        begin
            if(enable & outclk)
            begin
                if(timer == 0)
                begin
                    case(mode)
                        free_m:
                        begin
                            timer_next = timer - 1'b1;
                            timer_irq_next = 1'b1;
                        end
                        load_m:
                        begin
                            timer_next = load;
                            timer_irq_next = 1'b1;
                        end
                        capture_m:
                            timer_next = timer - 1'b1;
                        compare_m:
                            timer_next = timer - 1'b1;
                        pwm_m:
                        begin
                            timer_next = load;
                            pwm_next = 1'b0;
                        end
                        default:
                            timer_next = timer - 1'b1; 
                    endcase
                end
                
                else
                begin
                    case(mode)    
                        compare_m:
                            if(compare == timer)
                            begin
                                timer_next = 0;
                                timer_irq_next = 1'b1;
                            end
                            else
                                timer_next = timer - 1'b1;
                        pwm_m:
                            if(compare == timer)
                            begin
                                pwm_next = 1'b1;
                                timer_next =  timer - 1'b1;
                                timer_irq_next = 1'b1;
                            end
                            else
                                timer_next = timer - 1'b1;
                        default:
                            timer_next = timer - 1'b1; 
                    endcase
                end
            end
        
            else
            begin
                if(!enable)
                    state_next = idle_st;
            end    
        end
    endcase
  end
endmodule