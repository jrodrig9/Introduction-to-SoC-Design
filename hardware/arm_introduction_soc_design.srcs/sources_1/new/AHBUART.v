module AHBUART(
  input wire HCLK,
  input wire HRESETn,
  input wire [31:0] HADDR,
  input wire [31:0] HWDATA,
  input wire HREADY,
  input wire HWRITE,
  input wire [1:0] HTRANS,
  input wire HSEL,
  
  input  wire rx,
  output wire tx,
  output reg [31:0] HRDATA,
  output wire HREADYOUT,
  //UART Interrupt
  output wire uart_irq  //Interrupt
);

 //Internal AHB signals
  reg last_HWRITE;
  reg last_HSEL;
  reg [1:0] last_HTRANS;
  reg [31:0] last_HADDR;
  //Internal UART register
  reg [3:0] baudrate;
  reg parity;
  
  wire sel_baudrate;
  wire sel_parity;
  wire sel_status;
  wire add_tx_fifo;
  wire get_rx_fifo;
  wire [7:0] status;
  wire [7:0] rd_data;
  wire [7:0] wr_data;
  wire [7:0] tx_data;
  wire [7:0] rx_data;
  wire tx_full;
  wire tx_empty;
  wire rx_empty;
  wire rx_full;
  
  initial
  begin
    parity <= 0;
    baudrate <= 0;
  end
  
  assign wr_data = HWDATA[7:0];
  assign status = {6'b000000, tx_full,rx_empty};
  assign add_tx_fifo = (last_HADDR[7:0] == 8'h00) & last_HWRITE & last_HSEL & last_HTRANS[1];
  assign get_rx_fifo = (last_HADDR[7:0] == 8'h00) & ~last_HWRITE & last_HSEL & last_HTRANS[1];
  assign sel_status = (last_HADDR[7:0] == 8'h04) & ~last_HWRITE & last_HSEL & last_HTRANS[1];
  assign sel_baudrate = (last_HADDR[7:0] == 8'h08) & last_HSEL & last_HTRANS[1];
  assign sel_parity = (last_HADDR[7:0] == 8'h0c) & last_HSEL & last_HTRANS[1];
  assign uart_irq = ~rx_empty;

  always @(*)
  begin
    if(sel_baudrate & ~last_HWRITE)
        HRDATA = {28'b0000000000000000000000000000, baudrate};
    else if(sel_parity  & ~last_HWRITE)
        HRDATA = {31'b0000000000000000000000000000000, parity};
    else if(get_rx_fifo)
        HRDATA = {24'h000000, rd_data};
    else if(sel_status)
        HRDATA = {24'h000000, status};
    else
        HRDATA <= 0;    
  end  
  
  always @(posedge HCLK)
  if(HREADY)
    begin
      last_HADDR <= HADDR;
      last_HWRITE <= HWRITE;
      last_HSEL <= HSEL;
      last_HTRANS <= HTRANS;
    end
    
  always @(posedge HCLK, negedge HRESETn)
  begin
    if(!HRESETn)
      begin
        baudrate <= 0;
      end
      
    else if(HREADYOUT & last_HWRITE & sel_baudrate)
      begin
        baudrate <= HWDATA[3:0];
      end
  end
  
  always @(posedge HCLK, negedge HRESETn)
  begin
    if(!HRESETn)
      begin
        parity <= 0;
      end
      
    else if(HREADYOUT & last_HWRITE & sel_parity)
      begin
        parity <= HWDATA[0];
      end
  end
    
  assign HREADYOUT = ~tx_full;
   
  BAUDGEN uBAUDGEN(
    .clk(HCLK),
    .resetn(HRESETn),
    .baudrate(baudrate),
    .baudtick(b_tick)
  );
   
   //Transmitter FIFO
  FIFO  
   #(.DWIDTH(8), .AWIDTH(4))
	uFIFO_TX 
  (
    .clk(HCLK),
    .resetn(HRESETn),
    .rd(tx_done),
    .wr(add_tx_fifo),
    .w_data(wr_data[7:0]),
    .empty(tx_empty),
    .full(tx_full),
    .r_data(tx_data[7:0])
  );
  
   //RECEIVER FIFO
  FIFO  
   #(.DWIDTH(8), .AWIDTH(4))
	uFIFO_RX 
  (
    .clk(HCLK),
    .resetn(HRESETn),
    .rd(get_rx_fifo),
    .wr(rx_done),
    .w_data(rx_data[7:0]),
    .empty(rx_empty),
    .full(rx_full),
    .r_data(rd_data[7:0])
  );
  
  //UART transmitter
  UART_TX uUART_TX(
    .clk(HCLK),
    .resetn(HRESETn),
    .parity(parity),
    .tx_start(!tx_empty),
    .b_tick(b_tick),
    .d_in(tx_data[7:0]),
    .tx_done(tx_done),
    .tx(tx)
  );
  
  //UART receiver
  UART_RX uUART_RX(
    .clk(HCLK),
    .resetn(HRESETn),
    .parity(parity),
    .b_tick(b_tick),
    .rx(rx),
    .rx_done(rx_done),
    .dout(rx_data[7:0])
  );
  
endmodule
