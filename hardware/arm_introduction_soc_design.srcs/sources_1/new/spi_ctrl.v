module SPI_CTRL(
  input wire clk,
  input wire resetn,
  input wire s_tick,        //SPI generator tick
  input wire s_tick_mosi,
  input wire sclk_base,
  input wire spi_start,
  input wire [7:0] mosi_data, 
  input wire [7:0] mosi_size, 
  input wire [7:0] miso_size, 
  input wire miso,            
  
  output wire sclk,
  output wire mosi,
  output reg mosi_data_done,
  output reg miso_data_done,       //transfer completed
  output reg spi_done,
  output wire [7:0] miso_data    //output data
  );

//STATE DEFINES  
  localparam [2:0] idle_st = 3'b000;
  localparam [2:0] send_st = 3'b001;
  localparam [2:0] change_receive_st = 3'b011;
  localparam [2:0] receive_st = 3'b111;
  localparam [2:0] post_receive_st = 3'b110;
  localparam [2:0] change_idle_st = 3'b011;

//Internal Signals  
  reg [2:0] current_state;
  reg [2:0] next_state;
  reg [2:0] count_reg; //data-bit counter
  reg [2:0] count_next;
  reg [2:0] count_frame_reg; //data-bit counter
  reg [2:0] count_frame_next;
  reg [7:0] mosi_data_reg; //data register
  reg [7:0] mosi_data_next;
  reg [7:0] miso_data_reg; //data register
  reg [7:0] miso_data_next;
  reg mosi_reg;               //output data reg
  reg mosi_next;

  assign mosi = mosi_reg;
  assign miso_data = miso_data_reg;
  assign sclk = (current_state != idle_st ) ? sclk_base : 1'b0;
  
  initial
  begin
    mosi_data_done = 1'b0;
    miso_data_done = 1'b0;
    spi_done = 1'b0;
  end
  
  always @(posedge clk, negedge resetn) 
  begin
    if(!resetn) 
    begin
        current_state <= idle_st;
        count_reg <= 0;
        count_frame_reg <= 0;
        mosi_data_reg <= 0;
        miso_data_reg <= 0;
        mosi_reg <= 0;
    end
    
    else
    begin
        current_state <= next_state;
        count_reg <= count_next;
        count_frame_reg <= count_frame_next;
        mosi_data_reg <= mosi_data_next;
        miso_data_reg <= miso_data_next;
        mosi_reg <= mosi_next;
    end
  end
  
  always @(*)
  begin
    next_state = current_state;
    count_next = count_reg;
    mosi_next = mosi_reg;
    mosi_data_next = mosi_data_reg;
    miso_data_next = miso_data_reg;
    count_frame_next = count_frame_reg;
    mosi_data_done = 1'b0;
    miso_data_done = 1'b0;
    spi_done = 1'b0;
    
    case(current_state)
        idle_st:
        begin
            mosi_next = 1'b0;
            mosi_data_next = mosi_data;
            if(spi_start)
            begin
                if(s_tick_mosi)
                begin
                    next_state = send_st;
                    count_next = 0;
                    count_frame_next = 1;
                end  
            end
        end
        
        send_st:
        begin
            mosi_next = mosi_data_reg[7];
            if(s_tick_mosi)
            begin
                count_next = count_reg + 1'b1;
                mosi_data_next = mosi_data_reg << 1;
                if(count_reg == 6)
                begin
                    mosi_data_done = 1'b1;
                end
                else if(count_reg == 7)
                begin
                    if(count_frame_reg < mosi_size)
                    begin
                        count_next = 0;
                        mosi_data_next = mosi_data;
                        count_frame_next = count_frame_reg + 1'b1;
                    end
                    
                    else
                    begin
                        if(miso_size > 0)
                        begin
                            next_state = receive_st;
                            count_next = 0;
                            count_frame_next = 1;
                            next_state = receive_st;
                        end
                        
                        else
                        begin
                           next_state = idle_st;
                           spi_done = 1'b1;
                        end
                    end
                end
            end
        end
         
        receive_st:
        begin
            mosi_next = 1'b0;
            if(s_tick)
            begin
                miso_data_next = {miso, miso_data_reg[7:1]};
                count_next = count_reg + 1'b1;
                if(count_next == 7)
                begin
                    next_state = post_receive_st;
                end
            end
        end
        
        post_receive_st:
        begin
            if(s_tick)
            begin
                miso_data_done = 1'b1;
                if(count_frame_reg < miso_size)
                begin
                    count_next = 0;
                    count_frame_next = count_frame_reg + 1'b1;
                    next_state= receive_st;
                end
                    
                else
                begin
                    next_state = change_idle_st;
                    spi_done = 1'b1;
                end
            end
        end
        
        change_idle_st:
        begin
            if(s_tick_mosi)
            begin
                next_state = idle_st;
            end
        end
    endcase
  end
  
  endmodule