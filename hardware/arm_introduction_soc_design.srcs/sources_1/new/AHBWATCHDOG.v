module AHBWATCHDOG(
	//Inputs
    input wire HCLK,
	input wire HRESETn,
    input wire [31:0] HADDR,
    input wire [31:0] HWDATA,
    input wire [1:0] HTRANS,
    input wire HWRITE,
    input wire HSEL,
    input wire HREADY,
  
	//Output
    output reg [31:0] HRDATA,
    output wire HREADYOUT,
    output reg watchdog_reset
);

    localparam  idle_st =  1'b0;
    localparam  counter_st = 1'b1;
    
    //Internal AHB signals
    reg last_HWRITE;
    reg last_HSEL;
    reg [1:0] last_HTRANS;
    reg [31:0] last_HADDR;
    
    reg state;
    reg state_next;
    reg watchdog_reset_next;

    reg  [31:0] load;
    reg  [31:0] timer;
    reg  [31:0] timer_next;
    reg  [31:0] control;
    reg  clear;
    wire  reset_load;
    
    wire enable;
    wire  [3:0] mode;
    wire  [3:0] prescale;
    
    wire sel_load;
    wire sel_current;
    wire sel_control;
    wire sel_capture;
    wire sel_compare;
    wire sel_clear;
    wire capture_event;
    
    wire outclk;
        
    assign enable      = control[0];
    assign sel_load    = (last_HADDR[7:0] == 8'h00) & last_HSEL & last_HTRANS[1];
    assign sel_current = (last_HADDR[7:0] == 8'h04) & last_HSEL & last_HTRANS[1];
    assign sel_control = (last_HADDR[7:0] == 8'h08) & last_HSEL & last_HTRANS[1];
    assign sel_clear   = (last_HADDR[7:0] == 8'h0c) & last_HSEL & last_HTRANS[1];
    assign reset_load  = (HREADYOUT & last_HWRITE & sel_load);
    assign HREADYOUT   = 1'b1;
   
  always @(*)
  begin
    if(sel_current & ~last_HWRITE)
        HRDATA = timer;
    else if(sel_control  & ~last_HWRITE)
        HRDATA = control;
    else if(sel_load  & ~last_HWRITE)
        HRDATA = load;
    else
        HRDATA = 0;    
  end 
  
  prescaler uprescaler
  (
    .inclk(HCLK),
    .resetn(HRESETn),
    .prescale(4'b0000),
    .outclk(outclk)
  );
     
  always @(posedge HCLK)
  if(HREADY)
    begin
      last_HADDR <= HADDR;
      last_HWRITE <= HWRITE;
      last_HSEL <= HSEL;
      last_HTRANS <= HTRANS;
    end
    
  always @(posedge HCLK, negedge HRESETn)
  begin
    if(!HRESETn)
      begin
        load <= 0;
      end
      
    else if(HREADYOUT & last_HWRITE & sel_load)
      begin
        load <= HWDATA;
      end
  end
   
  always @(posedge HCLK, negedge HRESETn)
  begin
    if(!HRESETn)
      begin
        control <= 0;
      end
      
    else if(HREADYOUT & last_HWRITE & sel_control)
      begin
        control <= HWDATA;
      end
  end
  
  always @(posedge HCLK, negedge HRESETn)
  begin
    if(!HRESETn)
      begin
        clear <= 0;
      end
      
    else if(HREADYOUT & last_HWRITE & sel_clear)
      begin
        clear <= HWDATA[0];
      end
  end
  
  //Machine Status for timer modes
  always @(posedge HCLK, negedge HRESETn)
  begin
    if(!HRESETn)
      begin
        state <= 0;
        timer <= 0;
        watchdog_reset <= 1'b1;
      end
      
    else
      begin
        state <= state_next;
        timer <= timer_next;
        watchdog_reset <= watchdog_reset_next;
      end
  end
  
  //State logic
  always@(*)
  begin
    state_next = state;
    timer_next = timer;
    watchdog_reset_next = watchdog_reset;    
    case(state)
        idle_st:
        begin
            if(enable & outclk)
            begin
                state_next = counter_st;
                timer_next = 0;
            end
        end
    
        counter_st:
        begin
            if(enable & outclk)
            begin
                if(timer == 0)
                begin
                    timer_next = load;
                end
                
                else
                begin
                    if(reset_load)
                    begin
                        timer_next = load;
                    end
                    
                    else
                        timer_next = timer - 1'b1;
                        
                    if(timer_next == 0)
                        watchdog_reset_next = 1'b0;
                end 
            end
        
            else
            begin
                if(!enable)
                    state_next = idle_st;
            end    
        end
    endcase
  end
endmodule