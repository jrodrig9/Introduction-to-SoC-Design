module prescaler(
  input wire inclk,
  input wire resetn,
  input wire [3:0] prescale,
  output wire outclk
  );
  
  reg [7:0] prescale_clk;
  reg [7:0] count_reg;
  wire [7:0] count_next;
  
  assign count_next = ((count_reg != prescale_clk) ? count_reg + 1'b1 : 8'b00000000);
  assign outclk     = ((count_reg == prescale_clk) ? 1'b1 : 1'b0);
  
  always@*
  begin:DEC
    case(prescale)
      4'b0000: begin					
        prescale_clk = 1;
      end
      4'b0001: begin
         prescale_clk = 4;
      end
      4'b0010: begin
        prescale_clk = 16;
      end
      4'b0011: begin
        prescale_clk = 64;
      end
    endcase
  end
  
  always @ (posedge inclk, negedge resetn)
  begin
    if(!resetn)
        count_reg <= 8'b00000000;
    else
      count_reg <= count_next;
  end
  
  endmodule
