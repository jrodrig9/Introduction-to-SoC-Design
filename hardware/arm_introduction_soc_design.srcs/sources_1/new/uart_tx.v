module UART_TX(
  input wire clk,
  input wire resetn,
  input wire parity,
  input wire tx_start,        
  input wire b_tick,          //baud rate tick
  input wire [7:0] d_in,      //input data
  
  output reg tx_done,         //transfer finished
  output wire tx              //output data
  );
  
//STATE DEFINES  
  localparam [2:0] idle_st = 3'b000;
  localparam [2:0] start_st = 3'b001;
  localparam [2:0] data_st = 3'b011;
  localparam [2:0] stop_st = 3'b010;
  localparam [2:0] parity_st = 3'b110;
  
//Internal Signals  
  reg [2:0] current_state;
  reg [2:0] next_state;
  reg [3:0] b_reg;          //baud tick counter
  reg [3:0] b_next;
  reg [2:0] count_reg;      //data bit counter
  reg [2:0] count_next;
  reg [7:0] data_reg;       //data register
  reg [7:0] data_next;
  reg [3:0] parity_counter;
  reg [3:0] parity_counter_next;
  reg tx_reg;               //output data reg
  reg tx_next;
  wire  parity_data;

  assign tx = tx_reg;
  assign parity_data = parity_counter % 2;
  
  always @(posedge clk, negedge resetn) 
  begin
    if(!resetn) 
    begin
        current_state <= idle_st;
        b_reg <= 0;
        count_reg <= 0;
        data_reg <= 0;
        parity_counter <= 0;
        tx_reg <= 1'b1;
    end
    
    else
    begin
        current_state <= next_state;
        b_reg <= b_next;
        count_reg <= count_next;
        data_reg <= data_next;
        tx_reg <= tx_next;
        parity_counter <= parity_counter_next;
    end
  end
  
  always @(*)
  begin
    next_state = current_state;
    tx_done = 1'b0;
    b_next = b_reg;
    count_next = count_reg;
    data_next = data_reg;
    tx_next = tx_reg;
    parity_counter_next = parity_counter;
    
    case(current_state)
        idle_st:
        begin
            tx_next = 1'b1;
            if(tx_start)
            begin
                next_state = start_st;
                b_next = 0;
                data_next = d_in;
            end
        end
        
        start_st:
        begin
            tx_next = 1'b0;
            if(b_tick)
            begin
                if(b_reg == 15)
                begin
                    next_state = data_st;
                    b_next = 0;
                    count_next = 0;
                    parity_counter_next = 0;
                end
                
                else
                begin
                    b_next = b_reg + 1;
                end
            end
        end
        
        data_st:
        begin
            tx_next = data_reg[0];
            if(b_tick)
            begin
               if(b_reg == 15)
               begin
                b_next = 0;
                data_next = data_reg >> 1;
                parity_counter_next = parity_counter + data_next;
                if(count_reg == 7)
                begin
                    if(parity)
                        next_state = parity_st;
                    else
                        next_state = stop_st;
                end
                else
                    count_next = count_reg + 1;
               end
               else
                b_next = b_reg + 1;
            end
        end
               
        stop_st:
        begin
            tx_next = 1'b1;
            if(b_tick)
            begin
                if(b_reg == 15)
                begin
                    next_state = idle_st;
                    tx_done = 1'b1;
                end
                
                else
                    b_next = b_reg + 1;
            end
        end
        
        parity_st:
        begin
            tx_next = parity_data;
            if(b_tick)
            begin
                if(b_reg == 15)
                begin
                    next_state = stop_st;
                    b_next = 0;
                end
                
                else
                    b_next = b_reg + 1;
            end
        end
    endcase
  end
  
endmodule