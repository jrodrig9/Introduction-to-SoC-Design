module SPLITDEC(
  input wire [7:0] split,
  input wire [9:0] res_y,
    
  output reg [15:0] split_size,
  output reg [7:0] MAX_X,
  output reg [7:0] MAX_Y
    );
    
  always@*
  begin
    case(split)
      4'b0000: begin					
        split_size = 240;
        MAX_X = 240 / 8;
        MAX_Y = res_y / 16;
      end
      4'b0001: begin
        split_size = 200;
        MAX_X = 200 / 8;
        MAX_Y = res_y / 16;
      end
      4'b0010: begin
        split_size = 160;
        MAX_X = 160 / 8;
        MAX_Y = res_y / 16;
      end
      4'b0011: begin
        split_size = 360;
        MAX_X = 160 / 8;
        MAX_Y = res_y / 16;
      end
    endcase
  end
endmodule
