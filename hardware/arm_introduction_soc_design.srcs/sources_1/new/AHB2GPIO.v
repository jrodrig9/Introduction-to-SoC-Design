module AHBGPIO(
	//Inputs
    input wire HCLK,
	input wire HRESETn,
    input wire [31:0] HADDR,
    input wire [31:0] HWDATA,
    input wire [1:0] HTRANS,
    input wire HWRITE,
    input wire HSEL,
    input wire HREADY,
    input wire [3:0] gpio_in,
  
	//Output
	output reg [3:0] gpio_out,
    output reg [31:0] HRDATA,
    output wire HREADYOUT,
    output reg gpio_irq
);

    localparam  save_s    = 1'b0;
    localparam  compare_s    = 1'b1;

    //Internal AHB signals
    reg last_HWRITE;
    reg last_HSEL;
    reg [1:0] last_HTRANS;
    reg [31:0] last_HADDR;
    
    reg status;
    reg status_next;
    reg [3:0] last_gpio_in;
    reg gpio_irq_next;
    
    reg [3:0] direction;
    reg [3:0] mask;
    reg clear;

    wire sel_data;
    wire sel_direction;
    wire sel_mask;
    wire sel_clear;
    
    assign sel_data    = (last_HADDR[7:0] == 8'h00) & last_HSEL & last_HTRANS[1];
    assign sel_direction = (last_HADDR[7:0] == 8'h04) & last_HSEL & last_HTRANS[1];
    assign sel_mask = (last_HADDR[7:0] == 8'h08) & last_HSEL & last_HTRANS[1];
    assign sel_clear = (last_HADDR[7:0] == 8'h0c) & last_HSEL & last_HTRANS[1];
    assign HREADYOUT = 1'b1;
    
    always @(*)
    begin
        if(sel_data & ~last_HWRITE & !direction)
            HRDATA = {28'd0, gpio_in};
        else if(sel_data  & ~last_HWRITE & direction)
            HRDATA = {28'd0, (gpio_out & mask)};
        else if(sel_direction  & ~last_HWRITE)
            HRDATA = {28'd0, direction};
        else if(sel_mask  & ~last_HWRITE)
            HRDATA = {28'd0, mask};
        else
            HRDATA <= 0;    
    end
    
  always @(posedge HCLK)
  if(HREADY)
    begin
      last_HADDR <= HADDR;
      last_HWRITE <= HWRITE;
      last_HSEL <= HSEL;
      last_HTRANS <= HTRANS;
    end
        
  always @(posedge HCLK, negedge HRESETn)
  begin
    if(!HRESETn)
      begin
        gpio_out <= 0;
      end
      
    else if(HREADYOUT & last_HWRITE & sel_data & direction)
      begin
        gpio_out <= (HWDATA[3:0] & mask);
      end
  end
   
  always @(posedge HCLK, negedge HRESETn)
  begin
    if(!HRESETn)
      begin
        direction <= 0;
      end
      
    else if(HREADYOUT & last_HWRITE & sel_direction)
      begin
        direction <= HWDATA[3:0];
      end
  end
  
  always @(posedge HCLK, negedge HRESETn)
  begin
    if(!HRESETn)
      begin
        mask <= 4'b1111;
      end
      
    else if(HREADYOUT & last_HWRITE & sel_mask)
      begin
        mask <= HWDATA[3:0];
      end
  end
  
  always @(posedge HCLK, negedge HRESETn)
  begin
    if(!HRESETn)
      begin
        clear <= 1'b0;
      end
      
    else if(HREADYOUT & last_HWRITE & sel_clear)
      begin
        clear <= HWDATA[0];
      end
  end
  
  //IRQ LOGIC
  always @(posedge HCLK, negedge HRESETn)
  begin
    if(!HRESETn)
      begin
        status <= 1'b0;
        gpio_irq <= 1'b0;
      end
      
    else
      begin
        status <= status_next;
        gpio_irq <= gpio_irq_next;
      end
  end
  
  always @(*)
  begin
    status_next <= status;
    gpio_irq_next <= (clear)? 0 : gpio_irq;
    case(status)
        save_s:
        begin
            last_gpio_in <= gpio_in;
            status_next <= compare_s;
        end
        
        compare_s:
        begin
            if(last_gpio_in != gpio_in)
                gpio_irq_next <= 1'b1;
            status_next <= save_s;
       end
    endcase          
  end

endmodule