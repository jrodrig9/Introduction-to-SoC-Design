;------------------------------------------------------------------------------------------------------
; Design and Implementation of an AHB VGA Peripheral
; 1)Display text string: "TEST" on VGA. 
; 2)Change the color of the four corners of the image region.
;------------------------------------------------------------------------------------------------------

; Vector Table Mapped to Address 0 at Reset

						PRESERVE8
                		THUMB

        				AREA	RESET, DATA, READONLY	  			; First 32 WORDS is VECTOR TABLE
        				EXPORT 	__Vectors
					
__Vectors		    	DCD		0x00003FFC
        				DCD		Reset_Handler
        				DCD		0  			
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD 	0
        				DCD		0
        				DCD		0
        				DCD 	0
        				DCD		0
        				
        				; External Interrupts
						        				
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
              
                AREA |.text|, CODE, READONLY
;Reset Handler
Reset_Handler   PROC
                GLOBAL Reset_Handler
                ENTRY
				
;Write four white dots to four corners of the frame buffer

				LDR 	R1, =0x50000008
				MOVS	R0, #'T'
				STR		R0, [R1]

				LDR 	R1, =0x50000008
				MOVS	R0, #'E'
				STR		R0, [R1]
				
				LDR 	R1, =0x50000008
				MOVS	R0, #'S'
				STR		R0, [R1]
				
				LDR 	R1, =0x50000008
				MOVS	R0, #'T'
				STR		R0, [R1]
				
				LDR 	R1, =0x5000000C
				LDR		R0, =0xFF
				STR		R0, [R1]

				LDR 	R1, =0x50000194
				LDR		R0, =0xFF
				STR		R0, [R1]
		
				LDR 	R1, =0x5000EE0C
				LDR		R0, =0xFF
				STR		R0, [R1]

				LDR 	R1, =0x5000EF94
				LDR		R0, =0xFF
				STR		R0, [R1]
				
;Config UART buadrate
				
				LDR 	R1, =0x51000008
				LDR	    R0, =0x01
				STR		R0, [R1]
				
;Config UART parity 
				
				LDR 	R1, =0x5100000C
				LDR	    R0, =0x00
				STR		R0, [R1]
				
;Test UART TX
				
				LDR 	R1, =0x51000000
				MOVS	R0, #'H'
				STR		R0, [R1]

				LDR 	R1, =0x51000000
				MOVS	R0, #'E'
				STR		R0, [R1]
				
				LDR 	R1, =0x51000000
				MOVS	R0, #'L'
				STR		R0, [R1]
				
				LDR 	R1, =0x51000000
				MOVS	R0, #'L'
				STR		R0, [R1]
				
				LDR 	R1, =0x51000000
				MOVS	R0, #'O'
				STR		R0, [R1]
				
				LDR 	R1, =0x51000000
				MOVS	R0, #'\n'
				STR		R0, [R1]	
				B		GPIO
							
;Configure timer

				LDR 	R1, =0x52000000
				LDR	    R0, =0x64
				STR		R0, [R1]
				
				LDR 	R1, =0x52000008
				LDR	    R0, =0x65
				STR		R0, [R1]
				
				LDR		R0, [R1]
				
				LDR 	R1, =0x51000000
				STR		R0, [R1]
				
				LDR 	R1, =0x51000000
				MOVS	R0, #'\n'
				STR		R0, [R1]
				
				LDR 	R1, =0x52000010
				LDR 	R2, =0xFFFFFFFE
				STR		R2, [R1]
				
				LDR 	R1, =0x52000004
				LDR 	R5, =0x0000000F
				LDR     R2, =0x51000000

				
LOOP			
				LDR     R0, [R1]				
				ANDS 	R0, R0, R5
				STR		R0, [R2]
				B       LOOP
				
;Test GPIO

GPIO			LDR 	R1, =0x53000004
				LDR	    R0, =0x00
				STR		R0, [R1]
				
				LDR 	R1, =0x53000008
				LDR	    R0, =0x06
				STR		R0, [R1]
				
				LDR 	R1, =0x53000000
				LDR		R3, [R1]
				
				LDR 	R1, =0x53000004
				LDR	    R0, =0x01
				STR		R0, [R1]
				
				LDR 	R1, =0x53000000
				STR		R3, [R1]
				
				B       GPIO
				
					
				ENDP

				ALIGN 		4					 ; Align to a word boundary

		END                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
   