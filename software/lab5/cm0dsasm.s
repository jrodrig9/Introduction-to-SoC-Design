;------------------------------------------------------------------------------------------------------
; Design and Implementation of an AHB VGA Peripheral
; 1)Display text string: "TEST" on VGA. 
; 2)Change the color of the four corners of the image region.
;------------------------------------------------------------------------------------------------------

; Vector Table Mapped to Address 0 at Reset

						PRESERVE8
                		THUMB

        				AREA	RESET, DATA, READONLY	  			; First 32 WORDS is VECTOR TABLE
        				EXPORT 	__Vectors
					
__Vectors		    	DCD		0x00003FFC
        				DCD		Reset_Handler
        				DCD		0  			
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD 	0
        				DCD		0
        				DCD		0
        				DCD 	0
        				DCD		0
        				
        				; External Interrupts
						        				
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
              
                AREA |.text|, CODE, READONLY
;Reset Handler
Reset_Handler   PROC
                GLOBAL Reset_Handler
                ENTRY
				
;Write four white dots to four corners of the frame buffer
				LDR 	R1, =0x50000000
				LDR	    R0, =1
				STR		R0, [R1]
				
				LDR 	R1, =0x50000004
				LDR	    R0, =3
				STR		R0, [R1]
				
				LDR 	R1, =0x50000000
				LDR		R0, [R1]
				LDR 	R1, =0x50000008
				STR		R0, [R1]
				
				LDR 	R1, =0x50000004
				LDR		R0, [R1]
				LDR 	R1, =0x50000008
				STR		R0, [R1]
				
				LDR 	R1, =0x50000008
				MOVS	R0, #'T'
				STR		R0, [R1]

				LDR 	R1, =0x50000008
				MOVS	R0, #'E'
				STR		R0, [R1]
				
				LDR 	R1, =0x50000008
				MOVS	R0, #'S'
				STR		R0, [R1]
				
				LDR 	R1, =0x50000008
				MOVS	R0, #'T'
				STR		R0, [R1]
				
				LDR 	R1, =0x5000000C
				LDR		R0, =0xFF
				STR		R0, [R1]

				LDR 	R1, =0x50000194
				LDR		R0, =0xFF
				STR		R0, [R1]
		
				LDR 	R1, =0x5000EE0C
				LDR		R0, =0xFF
				STR		R0, [R1]

				LDR 	R1, =0x5000EF94
				LDR		R0, =0xFF
				STR		R0, [R1]
				
				
				LDR 	R1, =0x5000000C
				LDR		R0, =0xFF
				LDR 	R2, =0x00
				LDR     R4, =0x10
				LDR     R6, =2048
				LDR 	R7, =512
	
;Paint Rectangle
PAINT_LOOP		ADDS	R3, R1, R2
				STR     R0, [R3]
				ADDS    R2, R2, #4
				SUBS    R4, R4, #4
				BNE 	PAINT_LOOP
;NEXT_ROW						
				LDR     R5, =512
				LDR 	R2, =0x00
				LDR     R4, =0x10
				ADDS    R2, R2, R7
				ADDS    R7, R7, R5
				SUBS    R6, R6, R5
				BNE 	PAINT_LOOP

;Delay
				LDR   	R5, =200000
DELAY_LOOP		SUBS    R5, R5, #1
				BNE	    DELAY_LOOP
				
;Reset registers				
				ADDS	R1, R1, #4
				LDR 	R2, =0x00
				LDR     R4, =0x10
				LDR     R6, =2048
				LDR 	R7, =512
				B    PAINT_LOOP
				
				ENDP

				ALIGN 		4					 ; Align to a word boundary

		END                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
   