;------------------------------------------------------------------------------------------------------
; A Simple SoC  Application
; Toggle LEDs at a given frequency. 
;------------------------------------------------------------------------------------------------------



; Vector Table Mapped to Address 0 at Reset

						PRESERVE8
                		THUMB

        				AREA	RESET, DATA, READONLY	  			; First 32 WORDS is VECTOR TABLE
        				EXPORT 	__Vectors
					
__Vectors		    	DCD		0x00003FFC							; 1K Internal Memory
        				DCD		Reset_Handler
        				DCD		0  			
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD 	0
        				DCD		0
        				DCD		0
        				DCD 	0
        				DCD		0
        				
        				; External Interrupts
						        				
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
              
                AREA |.text|, CODE, READONLY
;Reset_Handler   PROC
;                GLOBAL Reset_Handler
;                ENTRY

;AGAIN		   	LDR 	R1, =0x50000004				;Write to LED with value 0x55
;				LDR		R0, =0xF0
;				STR		R0, [R1]



;				LDR		R0, =0x2FFFFF				;Delay
;Loop			SUBS	R0,R0,#1
;				BNE Loop

;				LDR 	R1, =0x50000008				;Write to LED with value 0xAA
;				LDR		R0, =0x00
;				STR		R0, [R1]

;				LDR		R0, =0x2FFFFF				;Delay
;Loop1			SUBS	R0,R0,#1
;				BNE Loop1

;				B AGAIN
;				ENDP


;				ALIGN 		4						; Align to a word boundary
;				END
;					

;Reset_Handler  PROC
;                GLOBAL Reset_Handler2
;                ENTRY

;AGAIN2		    LDR 	R1, =0x51000000				;Read to SWITCHs values.
;				LDR		R0, [R1]
;				
;				LDR 	R1, =0x50000000				;Write to LED with SWITCHs values
;				STR		R0, [R1]

;				B AGAIN2
;				ENDP


;				ALIGN 		4						; Align to a word boundary

;		END                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     

Reset_Handler   PROC
                GLOBAL Reset_Handler
                ENTRY

AGAIN		   	LDR 	R1, =0x50000004				;Write to LED with value 0x55
				LDR		R0, =0xFF
				STR		R0, [R1]

				LDR		R3, =0x2FFFFF				;Delay
Loop			SUBS	R3,R3,#1
				BNE     Loop

AGAIN2		    LDR 	R1, =0x50000000				;Write to LED with value 0xAA
				LDR		R0, [R1]
     		    LSRS  	R0,R0, #1
				STR		R0, [R1]

				LDR		R3, =0x2FFFFF				;Delay
Loop1			SUBS	R3,R3,#1
				BNE     Loop1
				
				SUBS	R3,R0,#1
				BNE     AGAIN2
				B       AGAIN
				ENDP

				ALIGN 		4						; Align to a word boundary
				END
	  