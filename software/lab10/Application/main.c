//------------------------------------------------------------------------------------------------------
// ARM CMSIS and Software Drivers
// 1)Input from 8-bit switch and output to LEDs
// 2)Input characters from keyboard (UART) and output to the terminal
// 3)A counter is incremented every second and displayed on the 7-segment display
//------------------------------------------------------------------------------------------------------

#include "EDK_CM0.h" 
#include "core_cm0.h"
#include "edk_driver.h"

#define Timer_Interrput_Frequency 			1
#define System_Tick_Frequency 					50000000
#define Timer_Prescaler 								1							//Timer Prescaler, options are: 256, 16, 1 
#define Timer_Load_Value 								(System_Tick_Frequency/Timer_Interrput_Frequency/Timer_Prescaler)


static char dig1,dig2,dig3,dig4;

//---------------------------------------------
// UART ISR
//---------------------------------------------

void UART_ISR(void)
{	
	char c;
	c=UART_read();		//read a character from UART
	UART_write(c);	//write the character to UART
}
 

//---------------------------------------------
// TIMER ISR
//---------------------------------------------


void Timer_ISR(void)
{
	char c;

	dig4++;
	
	if(dig4==10){
		dig4=0;
		dig3++;
		if (dig3==10){
			dig3=0;
			dig2++;
			if (dig2==10){
				dig2=0;
				dig1++;
			}
		}
	}
	
	c=' ';
	VGA_console(dig4);
	VGA_console(c);

	VGA_console(dig3);
	VGA_console(c);
	
	VGA_console(dig2);
	VGA_console(c);
	
	timer_irq_clear();
	
}	

//---------------------------------------------
// GPIO ISR
//---------------------------------------------

void GPIO_ISR(void)
{	
	char c;
	
	c='G';
	UART_write(c);
	c=' ';
	UART_write(c);
	
	GPIO_irq_clear();
}

//---------------------------------------------
// Main Function
//---------------------------------------------


int main(void){

	//Enter your code here
	NVIC_EnableIRQ(Timer_IRQn);
	NVIC_EnableIRQ(UART_IRQn);
	NVIC_EnableIRQ(GPIO_IRQn);
	NVIC_SetPriority(Timer_IRQn, 0x00000000);
	NVIC_SetPriority(UART_IRQn, 0x00000040);
	NVIC_SetPriority(GPIO_IRQn, 0x00000000);
	timer_init(Timer_Load_Value,1,0);
	timer_enable();
	GPIO_mask(15);
	
	dig1 = 0;
	dig2 = 0;
	dig3 = 0;
	dig4 = 0;
	
	while(1)
	{
				GPIO_write(GPIO_read());
	}
	
}


