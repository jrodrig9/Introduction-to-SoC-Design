//--------------------------------------------------------
// Peripheral driver functions
//--------------------------------------------------------


#include "EDK_CM0.h"
#include <string.h>
#include "edk_driver.h"

//---------------------------------------------
// VGA driver function
//---------------------------------------------

// Write character on console zone

void VGA_console (char character)
{
	VGA->CONSOLE = character;
}

//Plot a pixel to the image buffer

void VGA_plot_pixel (int x, int y, int col){
	//100x120 resolution
	int addr;
	addr=y*128+x;
	*(&(VGA->IMG)+addr) = col;
}

//---------------------------------------------
// UART driver function
//---------------------------------------------

//Configure UART baudrate

void UART_baudrate(int speed){
	
	UART->BAUDRATE = speed;
	
}

//Read character from UART

char UART_read(void){
	
	return (char) UART->DATA;
	
}

//Write charater on UART
void UART_write(char data)
{
	UART->DATA = (unsigned int) data;
}

//---------------------------------------------
// Timer driver function
//---------------------------------------------

//Timer initialization
//4-bits Control register: [0]: timer enable, [1] mode (free-run, reload, capture, compare and pwm) [2]: prescaler

void timer_init(int load_value, int prescale, int mode){
	
	unsigned int control ;
	int prescale_bits;

	switch(prescale)
	{
		case 1:
			prescale_bits=0x00;
			break;
		
		case 4:
			prescale_bits=0x01;
			break;
		
		case 16:
			prescale_bits=0x02;
			break;
		
		case 64:
			prescale_bits=0x03;
			break;
		
		default:
			prescale_bits=0x00;
			break;		
	}
	
	control = ((prescale_bits << 5)|(mode << 1));
	TIMER->INITVALUE = load_value;
	TIMER->CLEAR=1;
	TIMER->CONTROL = control;					
}

// timer enable

void timer_enable(void){
	int control ;
	control = TIMER->CONTROL;
	control = control | 0x01;
	TIMER->CONTROL = control;					
}

// clear interrupt request from timer

void timer_irq_clear(void){
	TIMER->CLEAR=0x01;					
}

//---------------------------------------------
// GPIO driver function
//---------------------------------------------

// GPIO read

int GPIO_read(void){
	GPIO->DIR=0;
	return GPIO->DATA;
}

// GPIO write

void GPIO_write(int data){
	GPIO->DIR=1;
	GPIO->DATA=data;
}

// GPIO set MASK

void GPIO_mask(int mask){

	GPIO->MASK = mask;
	
}

// GPIO clear IRQ flag

void GPIO_irq_clear(void){
	GPIO->CLEAR=0x01;					
}
