;------------------------------------------------------------------------------------------------------
; Design and Implementation of an AHB Interrupt Mechanism  
; 1)Input characters from keyboard (UART) and output to the terminal (using interrupt)
; 2)A counter is incremented from 1 to 10 and displayed on the 7-segment display (using interrupt)
;------------------------------------------------------------------------------------------------------



; Vector Table Mapped to Address 0 at Reset

						PRESERVE8
                		THUMB

        				AREA	RESET, DATA, READONLY	  			; First 32 WORDS is VECTOR TABLE
        				EXPORT 	__Vectors
					
__Vectors		    	DCD		0x00003FFC
        				DCD		Reset_Handler
        				DCD		0  			
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD 	0
        				DCD		0
        				DCD		0
        				DCD 	0
        				DCD		0
        				
        				; External Interrupts
						        				
        				DCD		Timer_Handler
        				DCD		UART_Handler
        				DCD		GPIO_Handler
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
        				DCD		0
              
                AREA |.text|, CODE, READONLY
;Reset Handler
Reset_Handler   PROC
                GLOBAL Reset_Handler
                ENTRY

                LDR     R1, =0xE000E400           ;Interrupt Priority Register
                LDR     R0, =0x00004000           ;Priority: IRQ0(Timer): 0x00, IRQ1(UART): 0x40
                STR     R0, [R1]
                LDR     R1, =0xE000E100           ;Interrupt Set Enable Register
                LDR     R0, =0x00000007           ;Enable interrupts for UART and timer 
                STR     R0, [R1]
		
				;Config UART buadrate
				
				LDR 	R1, =0x51000008
				LDR	    R0, =0x00
				STR		R0, [R1]
				
				;Config UART parity 
				
				LDR 	R1, =0x5100000C
				LDR	    R0, =0x00
				STR		R0, [R1]

				;Configure the timer
				
				LDR 	R1, =0x52000000		;timer load value register
				LDR 	R0, =0x02FAF080		;=50,000,000 (system tick frequency)
				STR		R0,	[R1]			
				LDR 	R1, =0x52000008		;timer control register
				MOVS	R0, #0x61			;prescaler=64, enable timer, reload mode
				STR		R0,	[R1]

                LDR     R5, =0x00000030		;counting-up counter, start from '0' (ascii=0x30)
				LDR     R4, =0x00000030		;counting-up counter, start from '0' (ascii=0x30)	

				;Configure the watchdog
				
				LDR		R1, =0x54000000
				LDR 	R0, =0x000000ff	
				STR		R0,	[R1]
				
				LDR 	R1, =0x54000008		;watchdog control register
				LDR 	R0, =0x00000001		;enable watchdog
				STR		R0,	[R1]
				
AGAIN			
				LDR		R1, =0x54000000
				LDR 	R0, =0x000000ff	
				STR		R0,	[R1]
				B		AGAIN		


				ENDP

Timer_Handler   PROC
				EXPORT Timer_Handler
			    
				PUSH    {R0,R1,R2,LR}
				
				;CLEAR TIMER_IRQ
				LDR     R1, = 0x52000014
				LDR 	R0, = 0x01
				STR 	R0, [R1]
				
				;PAINT ON VGA
				
				LDR     R1, = 0x50000008
				STR 	R5, [R1]

				;INCREMENT COUNTER
				
				ADDS    R5, R5, #1
				
				;DISABLE TIMER_IRQ
				
				CMP		R5,	#0x3A
				BNE		NEXT
				
				;DISABLE TIMER
				
				LDR 	R1, =0x52000008		;timer control register
				MOVS	R0, #0x00			;prescaler=64, enable timer, reload mode
				STR		R0,	[R1]
				
				;RESTORE TAIL
				
NEXT			MOVS	R0, #' '
				STR		R0, [R1]
				
			    POP     {R0,R1,R2,PC}                    ;return
                ENDP

UART_Handler    PROC
				EXPORT UART_Handler	
				
				PUSH    {R0,R1,R2,LR}
				
				;TEST UART IRQ
				
				LDR     R1, = 0x50000008
				MOVS	R0, #'V'
				STR 	R0, [R1]
				STR 	R4, [R1]
				MOVS	R0, #' '
				STR 	R0, [R1]
				
				;INCREMENT COUNTER
				
				ADDS    R4, R4, #1
				
				;NORMAL RUN
				
				LDR 	R1, =0x51000000
				MOVS	R0, #'U'
				STR		R0, [R1]
				
                LDR     R1, =0x51000000               ;UART
                LDR     R0, [R1]                      ;Get Data from UART
                STR     R0, [R1]                      ;Write to UART
                POP     {R0,R1,R2,PC}
				
                ENDP
					
GPIO_Handler    PROC
				EXPORT GPIO_Handler	
				
				PUSH    {R0,R1,R2,LR}
				
				;CLEAR IRQ
				
				LDR     R1, = 0x5300000C
				LDR 	R0, = 0x01
				STR 	R0, [R1]
				
				;TEST UART IRQ
				
				LDR     R1, = 0x50000008
				MOVS	R0, #'I'
				STR 	R0, [R1]
				MOVS	R0, #' '
				STR 	R0, [R1]
				
				;NORMAL RUN
				
				LDR 	R1, =0x51000000
				MOVS	R0, #'G'
				STR		R0, [R1]
				
                POP     {R0,R1,R2,PC}
				
                ENDP

				ALIGN 		4					 ; Align to a word boundary

		END                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    

				
